// import and assign express.Router()
const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");

// import verify and verifyAdmin from auth
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve All Users
router.get("/", userControllers.getAllUsers);

// Login
router.post("/login", userControllers.loginUser);

// Retrieve User Details
router.get("/getUserDetails", verify, userControllers.getUserDetails);

// Check If Email Exists (GET because we have to use req.body.)
router.post("/checkEmailExists", userControllers.checkEmailExists);

// Updating Regular User to Admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

// Update User Details (:id not required because the "verify" refers to the one being updated.)
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

// Enroll Registered User
router.post("/enroll", verify, userControllers.enroll);

// Get User's Enrollments
router.get("/getEnrollments", verify, userControllers.getEnrollments)

module.exports = router;