// import and assign express.Router()
const express = require('express');
const router = express.Router();

// import course controllers
const courseControllers = require('../controllers/courseControllers');

// import from auth
const auth = require('../auth')
// object destructuring of auth.js
// auth is an imported module, so therefore it is an object is JS.
const {verify, verifyAdmin} = auth;

// Routes

// Create/Add Course
router.post("/", verify, verifyAdmin, courseControllers.addCourse);

// Retrieve All Courses
router.get("/", courseControllers.getAllCourses);

// Retrieve Single Course
router.get("/getSingleCourse/:id", courseControllers.getSingleCourse);

// Archive Course
router.put("/archive/:id", verify, verifyAdmin, courseControllers.archiveCourse);

// Activate Course
router.put("/activate/:id", verify, verifyAdmin, courseControllers.activateCourse);

// Retrieve Active Courses
router.get("/getActiveCourses", courseControllers.getActiveCourses);

// Update Course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

// Retrieve Inactive Courses
router.get("/getInactiveCourses", verify, verifyAdmin, courseControllers.getInactiveCourses);

// Find Courses by Name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

// Find Courses by Price
router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice);

// Get Course Enrollees
router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees);

module.exports = router;