// import Course model
const Course = require("../models/Course");

// Controllers

// Create/Add Course
module.exports.addCourse = (req, res) => {

	// log info to check
	console.log(req.body);

	// create the new course
	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	});

	// save the new course
	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};

// Retrieve All Courses
module.exports.getAllCourses = (req, res) => {

	// use the find function with no parameters
	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Retrieve Single Course
module.exports.getSingleCourse = (req, res) => {

	// log info to check
	console.log(req.params);

	// use findById function
	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


// Archive Course
module.exports.archiveCourse = (req, res) => {

	// log info to check
	console.log(req.params.id);

	// create updates object
	let updates = {
		isActive : false
	}

	// find by id and update
	Course.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};


// Activate Course
module.exports.activateCourse = (req, res) => {

	// log info to check
	console.log(req.params.id);

	// create updates object
	let updates = {
		isActive : true
	}

	// find by id and update
	Course.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};


// Retrieve Active Courses
module.exports.getActiveCourses = (req, res) => {

	// find by isActive : true
	Course.find({isActive : true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


// Update Course
module.exports.updateCourse = (req, res) => {

	// log info to check
	console.log(req.params.id);
	console.log(req.body);

	// create updates object
	let updates = {
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	};

	// find by id and update
	Course.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};


// Retrieve Inactive Courses
module.exports.getInactiveCourses = (req, res) => {

	// find isActive : false
	Course.find({isActive : false})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Find Courses by Name
module.exports.findCoursesByName = (req, res) => {
	Course.find({name : {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if (result.length === 0) {
			return res.send("No courses found.");

		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// Find Courses by Price
module.exports.findCoursesByPrice = (req, res) => {
	Course.find({price : req.body.price})
	.then(result => {

		if (result.length === 0) {
			return res.send("No course found.");

		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// Get Course Enrollees
module.exports.getEnrollees = (req, res) => {

	Course.findById(req.params.id)
	.then(course => {

		// log info to check
		console.log(course);

		// return course.enrollees
		return res.send(course.enrollees);
	})
	.catch(err => res.send(err));
};