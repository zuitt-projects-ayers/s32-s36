// import bcrypt module
const bcrypt = require('bcrypt');

// import User model
const User = require("../models/User");

// import Course model
const Course = require("../models/Course");

// import auth module
const auth = require("../auth")

// Controllers

// User Registration

module.exports.registerUser = (req, res) => {

	// log info to check
	console.log(req.body);

	/*	Encrypt Password

		bcrypt - adds a layer of security to your user's password.

		bcrypt hashes our password into a randomized character version of the original string.

		syntax: bcrypt, hashSync(<stringToBeHashed>, <saltRounds>);

		Salt-Rounds are the the number of times the characters in the hash are randomized.
	*/
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	// Create a new user document out of our user model

	let newUser = new User({

		firstName : req.body.firstName,
		lastName : req.body.lastName,
		email : req.body.email,
		mobileNo : req.body.mobileNo,
		password : hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};


// Retrieve All Users

module.exports.getAllUsers = (req, res) => {
	
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


// Login

module.exports.loginUser = (req, res) => {

	// log info to check
	console.log(req.body);

	/*
		1. find by user email
		2. if a user email is found, check the password
		3. if we don't find the user email, send a message
		4. if upon checking, the found user's password is the same as our input password, we will generate a "key" to access our app. If not, we will turn him away by sending a message to the client.
	*/

	User.findOne({email : req.body.email})
	.then(foundUser => {

		if (foundUser === null) {
			return res.send("User does not exist.");

		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if (isPasswordCorrect) {
				return res.send({accessToken : auth.createAccessToken(foundUser)});

			} else {
				return res.send("Password is incorrect.");
			}
		}
	}).catch(err => res.send(err));
};


// Get User Details

module.exports.getUserDetails = (req, res) => {

	console.log(req.user)

	// 1. Find a logged-in user's document from our db and send it to the client by its id.

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


// Check If Email Exists
module.exports.checkEmailExists = (req, res) => {

	// log info to check
	console.log(req.body);

	// use User.findOne() with the email : req.body.email
	User.findOne({email : req.body.email})
	.then(result => {

		if (result === null) {
			return res.send("Email is available.")

		} else {
			return res.send("Email is already registered!\n" + result)
		}
	})
	.catch(err => res.send(err));
};


// Updating Regular User to Admin
module.exports.updateAdmin = (req, res) => {

	// log info to check
	console.log(req.user.id);
	// req.params.id captures the id
	console.log(req.params.id);

	// create updates object
	let updates = {
		isAdmin : true
	};

	// use findByIdAndUpdate(2 args and 1 option {new:true})
	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// Update User Details
module.exports.updateUserDetails = (req, res) => {

	// log info to check
	console.log(req.body); // check the input for new values for our user's details.
	console.log(req.user.id); // check the logged-in user's id.

	// create updates object
	let updates = {
		firstName : req.body.firstName,
		lastName : req.body.lastName,
		mobileNo : req.body.mobileNo
	}

	// find by id and update
	User.findByIdAndUpdate(req.user.id, updates, {new : true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// Enroll a User
module.exports.enroll = async (req, res) => {

	/*
		Enrollment Process:

		1. Look for the user by its id.
			>> Push the details of the course we're trying to enroll in.
				>> We'll push to a new enrollment subdocument in our user.

		2. Look for the course by its id.
			>> Push the details of the enrollee/user who's trying to enroll.
				>> We'll push to a new enrollees subdocument in our course.

		3. When both saving documents are successful, we send a message to the client.
	*/

	// log info to check
	console.log(req.user.id);
	console.log(req.body.courseId);

	// Checking if a user is an admin; if he is an admin, then he should not be able to enroll.
	if (req.user.isAdmin) {
		return res.send("Action Forbidden.");

	}

	/*
		Find the user:

		async - a keyword that allows us to make our function asynchronous, which means that, instead of JS behavior of running each code line by line, it will allow us to wait for the result of the function.

		await - a keyword that allows us to wait for a function to finish before proceeding (cannot be used without async.)
	*/
	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		// log info to check
		console.log(user);

		// add the courseId in an object and push that object into user's enrollment array
		let newEnrollment = {
			courseId : req.body.courseId
		};
		user.enrollments.push(newEnrollment);

		// save
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});

	// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with our message.
	if (isUserUpdated !== true) {
		return res.send({message : isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(req.body.courseId)
	.then(course => {

		// log info to check
		console.log(course);

		// create an object which will contain the user id of the enrollee of a course.
		let enrollee = {
			userID : req.user.id
		};
		course.enrollees.push(enrollee);

		// save
		return course.save()
		.then(course => true)
		.catch(err => err.message);
	});

	if (isCourseUpdated !== true) {
		return res.send({message : isCourseUpdated});
	}

	if (isUserUpdated && isCourseUpdated) {
		return res.send({message : 'Enrolled Successfully!'});
	}
};

// Get User's Enrollments
module.exports.getEnrollments = (req, res) => {

	User.findById(req.user.id)
	.then(user => {

		// log info to check
		console.log(user);

		// return user.enrollments
		return res.send(user.enrollments);
	})
	.catch(err => res.send(err));
};